---
layout: post
title: "GravityForms Variable Fees"
categories: tech, php, wordpress, gravityforms, jquery
comments: true
---
Gravity forms is great for quickly creating a payment form. Recently I was tasked with adding a variable fee to the form based on a user input payment field. This was fairly easy to implement using jQuery and the GravityForms `total` field;

~~~javascript
$('#gform_4').on('change', '#input_1_1', function(e) {
  var this_val = Number($(this).val().replace(/[^0-9\.]+/g,"")), // User input
  $fee_input = $('#field_1_2 input'), // The fee input field
  $fee_label = $('#input_1_2'); // The fee label

  fee_breakpoints = [300, 150, 25];

  if(this_val > fee_breakpoints[0]) { // Greater than 300
    fee = 10;
  } else if(this_val < fee_breakpoints[0] && this_val > fee_breakpoints[1]) { // Less than 300 && greater than 150
    fee = 7.50;
  } else if(this_val < fee_breakpoints[1] && this_val > fee_breakpoints[2]) { // Less than 150.01 && greater than 25
    fee = 6;
  } else if(this_val < fee_breakpoints[2]) { // Less than 25
    fee = 5;
  } else {
    fee = 0;
  }

  formatted_fee = "$" + fee;

  if($fee_input.val() != fee) {
    // Only set if it's not already set
    $fee_label.text(formatted_fee);
    $fee_input.val(fee);
    $(this).trigger('change');
  }
});
~~~

Using the above jQuery, whenever the input field is changed, the fee updates based on the field value. The `trigger('change')` at the end makes sure the [GravityForms](http://www.gravityforms.com/) javascript picks up on the update and adjusts the total value accordingly.