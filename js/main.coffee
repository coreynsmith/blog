---
---

nightOrDay = ->
  date = new Date()
  hours = date.getHours()

  if hours > 7 || hours < 20
    'day'
  else
    'night'

bodyTag = document.getElementsByTagName('body')[0]
bodyTag.className = nightOrDay()