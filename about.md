---
layout: page
title: About
permalink: /about/
---

![Corey Smith]( /assets/me.jpg ){: .alignleft .me-pic } Hi, I'm Corey, a full-stack developer from Shreveport, LA.  

I'm working at Bandwise as the Wordpress and Ruby on Rails developer/designer.  

Currently, I'm into Ruby, Rails, Unix and Javascript.  
