---
layout: post
title: "Polymorphic models in rails"
categories: tech, ruby, rails
---

Recently, I needed to create a polymorphic `Image` model to attach images to products. I add images with ajax, sending the referenced model name as a variable, like so:
~~~ruby
# params[:model] = 'model'
object = params[:model].constantize.find_by_id(params[:id].to_i)
~~~
That way, I can ajax upload an image, specifying the model with a simple string. 